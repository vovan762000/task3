/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jv21701.bondarenko.task3;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author vovan
 */
@Entity
@Table(name = "gruppa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Gruppa.findAll", query = "SELECT g FROM Gruppa g")
    , @NamedQuery(name = "Gruppa.findById", query = "SELECT g FROM Gruppa g WHERE g.id = :id")
    , @NamedQuery(name = "Gruppa.findByNumber", query = "SELECT g FROM Gruppa g WHERE g.number = :number")
    , @NamedQuery(name = "Gruppa.findBySpecialty", query = "SELECT g FROM Gruppa g WHERE g.specialty = :specialty")})
public class Gruppa implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "number")
    private int number;
    @Column(name = "specialty")
    private String specialty;

    public Gruppa() {
    }

    public Gruppa(Integer id) {
        this.id = id;
    }

    public Gruppa(Integer id, int number) {
        this.id = id;
        this.number = number;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Gruppa)) {
            return false;
        }
        Gruppa other = (Gruppa) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Gruppa{" + "id=" + id + ","
                + " number=" + number + ","
                + " specialty=" + specialty + '}';
    }

   
    
}
