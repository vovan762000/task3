/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jv21701.bondarenko.task3;

import java.util.List;

/**
 *
 * @author vovan
 */
public interface GruppaDAO {

    Gruppa create(int number,String specialty);

    Gruppa read(int id);

    void update(Gruppa gruppa,String specialty);

    void delete(Gruppa gruppa);

    List<Gruppa> getAll();
}
