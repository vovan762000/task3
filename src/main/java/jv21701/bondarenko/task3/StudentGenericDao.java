/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jv21701.bondarenko.task3;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

/**
 *
 * @author vovan
 */
public class StudentGenericDao implements GenericDao<Student> {

    EntityManager em;

    public StudentGenericDao(EntityManager em) {
        this.em = em;
    }

    @Override
    public Student create(Object... param) {
        Student student = new Student();
        EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        student.setFirstname((String) param[0]);
        student.setLastname((String) param[1]);
        student.setBirthDate((java.sql.Date) param[2]);
        student.setGroupId((Integer) param[3]);
        try {
            em.persist(student);
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
        }
        return student;

    }

    @Override
    public void update(Student object, Object... param) {
        EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        object.setGroupId((Integer) param[0]);
        try {
            em.merge(object);
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
        }
    }

    @Override
    public Student read(int id) {
        Student student = em.createNamedQuery("Student.findById", Student.class).
                setParameter("id", id).getSingleResult();
        return student;
    }

    @Override
    public List getAll() {
        Query q = em.createNamedQuery("Student.findAll", Student.class);
        List<Student> students = q.getResultList();
        return students;
    }

    @Override
    public void delete(Student object) {
        EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        try {
            em.remove(object);
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
        }
    }

}
