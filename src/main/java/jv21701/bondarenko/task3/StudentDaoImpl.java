/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jv21701.bondarenko.task3;

import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

/**
 *
 * @author vovan
 */
public class StudentDaoImpl implements StuedentDAO {
    
    private final EntityManager em;
    
    public StudentDaoImpl(EntityManager em) {
        this.em = em;
    }
    
    @Override
    public Student read(int id) {
        Student student = em.createNamedQuery("Student.findById", Student.class).
                setParameter("id", id).getSingleResult();
        return student;
    }
    
    @Override
    public void update(Student student, Integer groupId) {
        EntityTransaction t = em.getTransaction();
        t.begin();
        student.setGroupId(groupId);
        try {
            em.merge(student);
            t.commit();
        } catch (Exception e) {
            t.rollback();
        }
    }
    
    @Override
    public void delete(Student student) {
        EntityTransaction t = em.getTransaction();
        t.begin();
        try {
            em.remove(student);
            t.commit();
        } catch (Exception e) {
            t.rollback();
        }
    }
    
    @Override
    public List<Student> getAll() {
        Query q = em.createNamedQuery("Student.findAll", Student.class);
        List<Student> students = q.getResultList();
        return students;
    }
    
    @Override
    public Student create(String firstname, String lastname, Date birthDate, Integer groupId) {
        Student studentToSave = new Student();
        EntityTransaction t = em.getTransaction();
        t.begin();
        studentToSave.setFirstname(firstname);
        studentToSave.setLastname(lastname);
        studentToSave.setBirthDate(birthDate);
        studentToSave.setGroupId(groupId);
        try {
            em.persist(studentToSave);
            t.commit();
        } catch (Exception e) {
            t.rollback();
        }
        return studentToSave;
    }
    
}
