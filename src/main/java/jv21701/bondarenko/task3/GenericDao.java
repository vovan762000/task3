/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jv21701.bondarenko.task3;

import java.util.List;

/**
 *
 * @author vovan
 */
public interface GenericDao<T> {

    T create(Object... param);

    T read(int id);

    void update(T object, Object... param);

    void delete(T object);

    List<T> getAll();
}
