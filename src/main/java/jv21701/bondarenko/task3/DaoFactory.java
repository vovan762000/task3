/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jv21701.bondarenko.task3;

import javax.persistence.EntityManager;

/**
 *
 * @author vovan
 */
public interface DaoFactory {

    GruppaDAO getGruppaDAO(EntityManager em);

    StuedentDAO getStuedentDAO(EntityManager em);
}
