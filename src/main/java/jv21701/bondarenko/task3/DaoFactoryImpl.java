/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jv21701.bondarenko.task3;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author vovan
 */
public class DaoFactoryImpl implements DaoFactory{

    @Override
    public GruppaDAO getGruppaDAO(EntityManager em) {
        return new GruppaDaoImpl(em);
    }

    @Override
    public StuedentDAO getStuedentDAO(EntityManager em) {
        return new StudentDaoImpl(em);
    }

  
    
}
