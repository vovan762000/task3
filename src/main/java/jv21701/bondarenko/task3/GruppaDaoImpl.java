/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jv21701.bondarenko.task3;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

/**
 *
 * @author vovan
 */
public class GruppaDaoImpl implements GruppaDAO {

    private final EntityManager em;

    public GruppaDaoImpl(EntityManager em) {
        this.em = em;
    }

    @Override
    public Gruppa read(int id) {
        Gruppa gruppa = em.createNamedQuery("Gruppa.findById", Gruppa.class).
                setParameter("id", id).getSingleResult();
        return gruppa;
    }

    @Override
    public void update(Gruppa gruppa, String specialty) {
        EntityTransaction t = em.getTransaction();
        t.begin();
        gruppa.setSpecialty(specialty);
        try {
            em.merge(gruppa);
            t.commit();
        } catch (Exception e) {
            t.rollback();
        }
    }

    @Override
    public void delete(Gruppa gruppa) {
        EntityTransaction t = em.getTransaction();
        t.begin();
        try {
            em.remove(gruppa);
            t.commit();
        } catch (Exception e) {
            t.rollback();
        }
    }

    @Override
    public List<Gruppa> getAll() {
        Query q = em.createNamedQuery("Gruppa.findAll", Gruppa.class);
        List<Gruppa> gruppas = q.getResultList();
        return gruppas;
    }

    @Override
    public Gruppa create(int number, String specialty) {
        Gruppa gruppaToSave = new Gruppa();
        EntityTransaction t = em.getTransaction();
        t.begin();
        gruppaToSave.setNumber(number);
        gruppaToSave.setSpecialty(specialty);
        try {
            em.persist(gruppaToSave);
            t.commit();
        } catch (Exception e) {
            t.rollback();
        }
        return gruppaToSave;
    }

}
