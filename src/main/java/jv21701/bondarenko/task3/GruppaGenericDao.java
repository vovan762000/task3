/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jv21701.bondarenko.task3;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

/**
 *
 * @author vovan
 */
public class GruppaGenericDao implements GenericDao<Gruppa> {

    EntityManager em;

    public GruppaGenericDao(EntityManager em) {
        this.em = em;
    }

    @Override
    public Gruppa create(Object... param) {
        Gruppa gruppa = new Gruppa();
        EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        gruppa.setNumber((Integer) param[0]);
        gruppa.setSpecialty((String) param[1]);
        try {
            em.persist(gruppa);
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
        }
        return gruppa;
    }

    @Override
    public void update(Gruppa object, Object... param) {
        EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        object.setSpecialty((String) param[0]);
        try {
            em.merge(object);
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
        }
    }

    @Override
    public List<Gruppa> getAll() {
        Query q = em.createNamedQuery("Gruppa.findAll", Gruppa.class);
        List<Gruppa> grupps = q.getResultList();
        return grupps;
    }

    @Override
    public Gruppa read(int id) {
        Gruppa gruppa = em.createNamedQuery("Gruppa.findById", Gruppa.class).
                setParameter("id", id).getSingleResult();
        return gruppa;
    }

    @Override
    public void delete(Gruppa object) {
        EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        try {
            em.remove(object);
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
        }
    }

}
