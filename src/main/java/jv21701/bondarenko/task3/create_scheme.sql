CREATE SCHEMA `dao_example` DEFAULT CHARACTER SET utf8 ;
 CREATE TABLE IF NOT EXISTS
 `dao_example`.`gruppa` ( `id` INT NOT NULL AUTO_INCREMENT ,
 `number` INT NOT NULL ,
 `specialty` VARCHAR(45) NULL ,
 PRIMARY KEY (`id`) );
 CREATE TABLE IF NOT EXISTS
 `dao_example`.`Student` ( `id` INT NOT NULL AUTO_INCREMENT ,
 `firstname` VARCHAR(45) NULL ,
 `lastname` VARCHAR(45) NULL ,
 `birth_date` DATE NULL ,
 `group_id` INT , PRIMARY KEY (`id`) );
