/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jv21701.bondarenko.task3;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author vovan
 */
public class App {
    
    public static void main(String[] args) {
        EntityManagerFactory emFactory = Persistence.createEntityManagerFactory("jv21701.bondarenko_Task3_jar_1.0-SNAPSHOTPU");
        EntityManager em = emFactory.createEntityManager();
        DaoFactory daoFactory = (DaoFactory) new DaoFactoryImpl();
        GruppaDAO gruppaDAO = daoFactory.getGruppaDAO(em);
        StuedentDAO stuedentDAO = daoFactory.getStuedentDAO(em);
        System.out.println(stuedentDAO.read(6));
        
        for (Student arg : stuedentDAO.getAll()) {
            System.out.println(arg);
        }
        
        GenericDao gruppa = new GruppaGenericDao(em);
        System.out.println(gruppa.read(3));
        for (Object arg : gruppa.getAll()) {
            System.out.println(arg);
        }
        
        em.close();
        emFactory.close();
        
    }
}
