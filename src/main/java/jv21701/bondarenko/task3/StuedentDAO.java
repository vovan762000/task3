/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jv21701.bondarenko.task3;

import java.util.Date;
import java.util.List;

/**
 *
 * @author vovan
 */
public interface StuedentDAO {

    Student create(String firstname,String lastname,Date birthDate,Integer groupId);

    Student read(int id);

    void update(Student student,Integer groupId);

    void delete(Student student);

    List<Student> getAll();
}
